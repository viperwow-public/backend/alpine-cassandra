### What is it?
This is the unofficial image of the Apache Cassandra database

### Main goal
The main goal is to provide the closest equivalent to the official image, but based on Alpine Linux

### Usage
**How to create docker image:**
- Run command: *docker build -t alpine-cassandra .*

**How to create docker container out of image:**
- Run command: *docker run -itd -p 7000:7000 -p 7001:7001 -p 7199:7199 -p 9042:9042 -p 9160:9160 --name alpine-cassandra alpine-cassandra:latest*
