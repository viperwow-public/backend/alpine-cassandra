# This image is heavily inspired by
# https://github.com/docker-library/cassandra/tree/064fb4e2682bf9c1909e4cb27225fa74862c9086/3.11
# https://github.com/Nebo15/alpine-cassandra
FROM alpine:latest

ENV CASSANDRA_USER=cassandra \
    CASSANDRA_GROUP=cassandra

# Create a user and group
RUN addgroup -S $CASSANDRA_GROUP && adduser -S $CASSANDRA_USER -G $CASSANDRA_GROUP

# Install gosu (executing commands under another user becomes better)
ENV GOSU_VERSION=1.10
RUN set -x \
    && apk add --no-cache --virtual .gosu-deps \
        dpkg \
        gnupg \
        openssl \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true \
    apk del --purge .gosu-deps

# Set cassandra-related paths and version
ENV CASSANDRA_VERSION=3.11.4 \
    CASSANDRA_HOME=/opt/cassandra \
    CASSANDRA_CONFIG=/etc/cassandra \
    CASSANDRA_PERSIST_DIR=/var/lib/cassandra \
    CASSANDRA_LOG=/var/log/cassandra

# Cassandra 3.0 and later require Java 8u40 or later
# Also, we need bash for scripts execution
RUN apk --update --no-cache add \
    openjdk8-jre \
	bash

# Change ownage and access rights of the working folders
RUN mkdir -p $CASSANDRA_PERSIST_DIR $CASSANDRA_CONFIG $CASSANDRA_LOG  \
	&& chown -R $CASSANDRA_USER:$CASSANDRA_GROUP $CASSANDRA_PERSIST_DIR $CASSANDRA_CONFIG $CASSANDRA_LOG \
	&& chmod 777 $CASSANDRA_PERSIST_DIR $CASSANDRA_CONFIG $CASSANDRA_LOG

# Install Cassandra
RUN apk --update --no-cache add --virtual .cassandra-build-deps \
        wget \
        ca-certificates \
        tar \
    && wget http://artfiles.org/apache.org/cassandra/${CASSANDRA_VERSION}/apache-cassandra-${CASSANDRA_VERSION}-bin.tar.gz -P /tmp \
    && tar -xvzf /tmp/apache-cassandra-${CASSANDRA_VERSION}-bin.tar.gz -C /tmp/ \
    && mv /tmp/apache-cassandra-${CASSANDRA_VERSION} ${CASSANDRA_HOME} \
    && mv ${CASSANDRA_HOME}/conf/* ${CASSANDRA_CONFIG} \
    && chown -R $CASSANDRA_USER:$CASSANDRA_GROUP $CASSANDRA_HOME \
    && chmod 777 $CASSANDRA_HOME \
    && apk del --purge .cassandra-build-deps \
    && rm -rf /tmp/apache-cassandra-${CASSANDRA_VERSION}-bin.tar.gz

# Add path to cassandra bin to PATH
# https://stackoverflow.com/questions/44062812/cassandra-nodetool-error
# We have to map our configuration path with cassandra's default one and there is a bit more info below
# https://fossies.org/diffs/apache-cassandra/3.9-src_vs_3.10-src/bin/nodetool-diff.html
ENV PATH=$PATH:${CASSANDRA_HOME}/bin \
    CASSANDRA_CONF=${CASSANDRA_CONFIG}

# Get initial shell script
COPY docker-entrypoint.sh /usr/local/bin/

# Fix java-related issues
RUN set -ex; \
	\
	dpkgArch="$(dpkg --print-architecture)"; \
	case "$dpkgArch" in \
		ppc64el) \
# https://issues.apache.org/jira/browse/CASSANDRA-13345
# "The stack size specified is too small, Specify at least 328k"
			if grep -q -- '^-Xss' "$CASSANDRA_CONFIG/jvm.options"; then \
# 3.11+ (jvm.options)
				grep -- '^-Xss256k$' "$CASSANDRA_CONFIG/jvm.options"; \
				sed -ri 's/^-Xss256k$/-Xss512k/' "$CASSANDRA_CONFIG/jvm.options"; \
				grep -- '^-Xss512k$' "$CASSANDRA_CONFIG/jvm.options"; \
			elif grep -q -- '-Xss256k' "$CASSANDRA_CONFIG/cassandra-env.sh"; then \
# 3.0 (cassandra-env.sh)
				sed -ri 's/-Xss256k/-Xss512k/g' "$CASSANDRA_CONFIG/cassandra-env.sh"; \
				grep -- '-Xss512k' "$CASSANDRA_CONFIG/cassandra-env.sh"; \
			fi; \
			;; \
	esac; \
	\
# https://issues.apache.org/jira/browse/CASSANDRA-11661
	sed -ri 's/^(JVM_PATCH_VERSION)=.*/\1=25/' "$CASSANDRA_CONFIG/cassandra-env.sh"

# Backwards compat
RUN chmod 777 /usr/local/bin/docker-entrypoint.sh \
    && ln -s usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

VOLUME $CASSANDRA_PERSIST_DIR

# 7000: intra-node communication
# 7001: TLS intra-node communication
# 7199: JMX
# 9042: CQL
# 9160: thrift service
EXPOSE 7000 7001 7199 9042 9160
CMD ["cassandra", "-f"]